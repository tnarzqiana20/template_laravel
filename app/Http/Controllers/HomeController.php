<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    //
    public function daftar(Request $request)
    {
        // dd($request);
        $nama = $request['first_name'];
        $panjang = $request['last_name'];
        return view('home.index', compact('nama', 'panjang'));
    }
    public function table()
    {
        return view('table.table');
    }
    public function data_table()
    {
        return view('table.data-table');
    }
}
