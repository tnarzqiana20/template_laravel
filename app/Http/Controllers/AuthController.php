<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function welcome()
    {
        return view('auth.index');
    }
    public function register()
    {
        return view('auth.register');
    }
}
