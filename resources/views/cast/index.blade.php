@extends('layout.global')
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.css"/>
@endpush
@section('title')
Data Cast
@endsection
@section('content')
<a href="{{ route('cast.create') }}" class="btn btn-primary mb-3">Tambah</a>
<table id="example1" class="table table-bordered table-striped">
    <thead class="thead-light">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Nama</th>
            <th scope="col">Usia</th>
            <th scope="col">Bio</th>
            <th scope="col">Actions</th>
        </tr>
    </thead>
    <tbody>
        @forelse ($cast as $key=>$value)
            <tr>
                <td>{{$key + 1}}</th>
                <td>{{$value->nama}}</td>
                <td>{{$value->umur}}</td>
                <td>{{$value->bio}}</td>
                <td>
                    <a href="{{ route('cast.show', [$value->id]) }}" class="btn btn-info">Show</a>
                    <a href="{{ route('cast.edit', [$value->id]) }}" class="btn btn-primary">Edit</a>
                    <form action="{{ route('cast.destroy', [$value->id]) }}" method="POST">
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
        @empty
            <tr colspan="3">
                <td>No data</td>
            </tr>  
        @endforelse              
    </tbody>
</table>  
@endsection
@push('script')
<script src="{{ asset('asset/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('asset/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush