@extends('layout.global')
@section('title')
Tambah Data
@endsection
@section('content')

<div>
    <form action="{{ route('cast.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="nama">Nama</label>
            <input type="text" class="form-control" name="nama" id="nama" placeholder="Masukkan Nama" required autofocus>
            @error('nama')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="usia">Usia</label>
            <input type="number" class="form-control" name="umur" id="usia" placeholder="Masukkan Nama" required>
            @error('usia')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <div class="form-group">
            <label for="bio">Bio</label>
            <textarea class="form-control" name="bio" id="bio" rows="3" placeholder="Bio" required></textarea>
            @error('bio')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Tambah</button>
        <a href="{{ route('cast.index') }}" class="btn btn-warning">Kelmbali</a>
    </form>
</div>
@endsection
